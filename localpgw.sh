#!/usr/bin/env bash
set -euo pipefail

echo "Starting Pushgateway..."
docker rm -f dev-pgw &> /dev/null || :
docker run --cap-add=IPC_LOCK -d --name=dev-pgw -p 9091:9091 prom/pushgateway:v1.0.0 &> /dev/null
echo "...done. Run \"docker rm -f dev-pgw\" to clean up the container."
echo
