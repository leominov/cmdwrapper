package main

import (
	"os"
	"testing"
)

func TestLoadFromEnv(t *testing.T) {
	c := &Config{}
	c.LoadFromEnv()
}

func TestString(t *testing.T) {
	c := &Config{}
	if len(c.String()) == 0 {
		t.Error("Must be non-zero length")
	}
}

func TestParse(t *testing.T) {
	c := &Config{
		PushgatewayURL: "http://127.0.0.1:9090",
		Job:            "",
	}
	if err := c.Parse(); err == nil {
		t.Error("Must be an error")
	}
	c = &Config{
		PushgatewayURL: "http://127.0.0.1:9090",
		Job:            "!.",
	}
	if err := c.Parse(); err == nil {
		t.Error("Must be an error")
	}
	c = &Config{
		PushgatewayURL: "http://127.0.0.1:9090",
		Job:            "job",
		Namespace:      "!.",
	}
	if err := c.Parse(); err == nil {
		t.Error("Must be an error")
	}
	c = &Config{
		PushgatewayURL: "http://127.0.0.1:9090",
		Job:            "job",
		Namespace:      "",
	}
	c.Parse()
	if c.Job != c.Namespace {
		t.Error("Must be same")
	}
	c = &Config{
		Job:            "job",
		PushgatewayURL: ":",
	}
	if err := c.Parse(); err == nil {
		t.Error("Must be an error")
	}
	c = &Config{
		Job:            "job",
		PushgatewayURL: "http://127.0.0.1:9090",
		Timeout:        "1d",
	}
	if err := c.Parse(); err == nil {
		t.Error("Must be an error")
	}
	c = &Config{
		Job:            "job",
		PushgatewayURL: "http://127.0.0.1:9090",
		Timeout:        "1h",
		Labels:         "f-o-o=bar",
	}
	if err := c.Parse(); err == nil {
		t.Error("Must be an error")
	}
	c = &Config{
		Job:            "job",
		PushgatewayURL: "http://127.0.0.1:9090",
		Timeout:        "1h",
		Labels:         "foo=bar",
	}
	if err := c.Parse(); err != nil {
		t.Error(err)
	}
	c = &Config{
		Job:            "job",
		PushgatewayURL: "http://127.0.0.1:9090",
		Labels:         "foo=bar",
	}
	if err := c.Parse(); err != nil {
		t.Error(err)
	}
}

func TestNewConfig(t *testing.T) {
	os.Setenv(jobEnv, "job")
	os.Setenv(pushgatewayEnv, "http://127.0.0.1:9090")
	os.Setenv(timeoutEnv, "1d")
	if _, err := NewConfig(); err == nil {
		t.Error("Must be an error")
	}
	os.Setenv(timeoutEnv, "1h")
	if _, err := NewConfig(); err != nil {
		t.Error(err)
	}
}
