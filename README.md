# cmdwrapper

[![Build Status](https://travis-ci.com/leominov/cmdwrapper.svg?branch=master)](https://travis-ci.com/leominov/cmdwrapper)
[![codecov](https://codecov.io/gh/leominov/cmdwrapper/branch/master/graph/badge.svg)](https://codecov.io/gh/leominov/cmdwrapper)

Report information about Shell-command execution to [Pushgateway](https://github.com/prometheus/pushgateway) and standardize log format.

## Environment variables

* `CMDWRAPPER_PG_URL` – e.g. `http://127.0.0.1:9091` (if not defined, metrics will not be pushed);
* `CMDWRAPPER_PG_JOB` – e.g. `backup` (required for `CMDWRAPPER_PG_URL`);
* `CMDWRAPPER_PG_NAMESPACE` – e.g. `my_backup` (if not defined, `CMDWRAPPER_PG_JOB` will be used);
* `CMDWRAPPER_PG_LABELS` – e.g. `env=prod,group=devops`;
* `CMDWRAPPER_PG_INSTANCE` – e.g. `localhost` (if not defined, OS hostname will be used);
* `CMDWRAPPER_TIMEOUT` – e.g. `1h` (if not defined, `1h` will be used);
* `CMDWRAPPER_OUTPUT` – Output format – `json` or `text` (if not defined, `json` will be used).

## Metrics

* `${namespace}_last_duration_seconds` – The duration of the last completion in seconds;
* `${namespace}_last_completion_successful` – The result of the last completion;
* `${namespace}_last_success_timestamp_seconds` – The timestamp of the last successful completion;
* `${namespace}_last_completion_timestamp_seconds` – The timestamp of the last completion, successful or not.

## Usage

```shell
$ ./localpgw.sh
$ export CMDWRAPPER_PG_JOB=backup
$ export CMDWRAPPER_PG_LABELS=env=prod,service=postgres
$ export CMDWRAPPER_PG_URL=http://127.0.0.1:9091
$ cmdwrapper echo "Hello"
{"env":"prod","hostname":"localhost","job":"backup","level":"info","msg":"Starting backup...","pid":1,"service":"postgres","time":"2019-05-18T11:04:33+05:00"}
{"env":"prod","hostname":"localhost","job":"backup","level":"info","msg":"Hello","pid":1,"service":"postgres","time":"2019-05-18T11:04:33+05:00"}
{"duration":2.307434,"env":"prod","hostname":"localhost","job":"backup","level":"info","msg":"Done","pid":1,"service":"postgres","time":"2019-05-18T11:04:33+05:00"}
```

![Result](https://api.monosnap.com/file/download?id=BuzJPCI4c76h5KCNeqLG9pyld4N0Ob)
