package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	jobEnv         = "CMDWRAPPER_PG_JOB"
	namespaceEnv   = "CMDWRAPPER_PG_NAMESPACE"
	pushgatewayEnv = "CMDWRAPPER_PG_URL"
	labelsEnv      = "CMDWRAPPER_PG_LABELS"
	instanceEnv    = "CMDWRAPPER_PG_INSTANCE"
	timeoutEnv     = "CMDWRAPPER_TIMEOUT"
	outputEnv      = "CMDWRAPPER_OUTPUT"

	defaultTimeout = "1h"
)

var (
	nameRegExp = regexp.MustCompile(`^([a-zA-Z0-9_]*)$`)
)

type Config struct {
	Job            string
	PushgatewayURL string
	Labels         string
	Timeout        string
	Namespace      string
	Instance       string
	timeout        time.Duration
	labels         map[string]string
}

func NewConfig() (*Config, error) {
	c := &Config{}
	c.LoadFromEnv()
	if err := c.Parse(); err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Config) LoadFromEnv() {
	c.Job = os.Getenv(jobEnv)
	c.PushgatewayURL = os.Getenv(pushgatewayEnv)
	c.Labels = os.Getenv(labelsEnv)
	c.Timeout = os.Getenv(timeoutEnv)
	c.Namespace = os.Getenv(namespaceEnv)
	c.Instance = os.Getenv(instanceEnv)
}

func (c *Config) Parse() error {
	c.labels = parseLabels(c.Labels)
	for label := range c.labels {
		if !nameRegExp.MatchString(label) {
			return fmt.Errorf("Label %s does not match to %s", label, nameRegExp)
		}
	}
	if len(c.PushgatewayURL) > 0 {
		_, err := url.Parse(c.PushgatewayURL)
		if err != nil {
			return fmt.Errorf("Failed to parse PushgatewayURL: %v", err)
		}
		if len(c.Job) == 0 {
			return errors.New("Job must be specified")
		}
		if !nameRegExp.MatchString(c.Job) {
			return fmt.Errorf("Job %s does not match to %s", c.Job, nameRegExp)
		}
		if len(c.Namespace) == 0 {
			c.Namespace = c.Job
		} else {
			if !nameRegExp.MatchString(c.Namespace) {
				return fmt.Errorf("Namespace %s does not match to %s", c.Namespace, nameRegExp)
			}
		}
	}
	if len(c.Timeout) == 0 {
		c.Timeout = defaultTimeout
	}
	d, err := time.ParseDuration(c.Timeout)
	if err != nil {
		return fmt.Errorf("Failed to parse Timeout: %v", err)
	}
	c.timeout = d
	if len(c.Instance) == 0 {
		c.Instance, _ = os.Hostname()
	}
	return nil
}

func (c *Config) String() string {
	out, _ := json.Marshal(c)
	return string(out)
}

func (c *Config) LogFields() logrus.Fields {
	fields := logrus.Fields{
		"pid":      os.Getpid(),
		"hostname": c.Instance,
		"job":      c.Job,
	}
	for k, v := range c.labels {
		fields[k] = v
	}
	return fields
}

func parseLabels(raw string) map[string]string {
	out := make(map[string]string)
	labelsKV := strings.Split(raw, ",")
	for _, labelKV := range labelsKV {
		labelKV = strings.TrimSpace(labelKV)
		label := strings.Split(labelKV, "=")
		if len(label) != 2 {
			continue
		}
		name := strings.TrimSpace(label[0])
		value := strings.TrimSpace(label[1])
		out[name] = value
	}
	return out
}
