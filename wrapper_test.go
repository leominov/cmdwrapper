package main

import (
	"net/http"
	"os"
	"testing"

	"github.com/sirupsen/logrus"
)

var (
	envs = map[string]bool{
		jobEnv:         true,
		namespaceEnv:   true,
		pushgatewayEnv: true,
		labelsEnv:      true,
		timeoutEnv:     true,
	}
)

func flushEnv() {
	for k := range envs {
		os.Unsetenv(k)
	}
}

func TestRun(t *testing.T) {
	flushEnv()
	testRun1(t)

	go func() {
		mux := http.NewServeMux()
		mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(202)
		})
		err := http.ListenAndServe(":9090", mux)
		if err != nil {
			t.Fatal(err)
		}
	}()

	flushEnv()
	testRun2(t)
}

func testRun1(t *testing.T) {
	os.Setenv(jobEnv, "job")
	os.Setenv(pushgatewayEnv, "http://127.0.0.1:9090")
	os.Setenv(timeoutEnv, "1h")
	c, err := NewConfig()
	if err != nil {
		t.Error(err)
	}
	w := NewWrapper(logrus.New().WithField("foo", "bar"), c)
	_, err = w.Run([]string{})
	if err == nil {
		t.Error("Must be an error")
	}
	_, err = w.Run([]string{"whoami"})
	if err == nil {
		t.Error("Must be an error")
	}
	_, err = w.Run([]string{"echo", "foobar"})
	if err == nil {
		t.Error("Must be an error")
	}
	_, err = w.Run([]string{"foobar"})
	if err == nil {
		t.Error("Must be an error")
	}
}

func testRun2(t *testing.T) {
	os.Setenv(jobEnv, "job")
	os.Setenv(pushgatewayEnv, "http://127.0.0.1:9090")
	os.Setenv(labelsEnv, "env=prod")
	os.Setenv(timeoutEnv, "0.1s")
	c, err := NewConfig()
	if err != nil {
		t.Error(err)
	}
	w := NewWrapper(logrus.New().WithField("foo", "bar"), c)
	_, err = w.Run([]string{})
	if err == nil {
		t.Error("Must be an error")
	}
	_, err = w.Run([]string{"whoami"})
	if err != nil {
		t.Error("Must be a nil")
	}
	_, err = w.Run([]string{"sleep", "0.5"})
	if err == nil {
		t.Error("Must be an error")
	}
	_, err = w.Run([]string{"foobar"})
	if err == nil {
		t.Error("Must be an error")
	}
}
