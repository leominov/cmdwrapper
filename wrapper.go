package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"github.com/sirupsen/logrus"
)

type Wrapper struct {
	log                  *logrus.Entry
	pushMetrics          bool
	pusher               *push.Pusher
	c                    *Config
	durationMetric       prometheus.Gauge
	resultMetric         prometheus.Gauge
	successTimeMetric    prometheus.Gauge
	completionTimeMetric prometheus.Gauge
}

func NewWrapper(log *logrus.Entry, c *Config) *Wrapper {
	wrapper := &Wrapper{
		log: log,
		c:   c,
	}
	wrapper.pushMetrics = true
	if len(c.PushgatewayURL) == 0 {
		wrapper.log.Warn("Empty Pushgateway URL, metrics will not be pushed")
		wrapper.pushMetrics = false
	}
	pusher := push.New(c.PushgatewayURL, c.Job)
	for k, v := range c.labels {
		pusher = pusher.Grouping(k, v)
	}
	pusher = pusher.Grouping("instance", c.Instance)
	wrapper.pusher = pusher
	wrapper.registerMetrics()
	return wrapper
}

func (w *Wrapper) registerMetrics() {
	w.durationMetric = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: w.c.Namespace,
		Name:      "last_duration_seconds",
		Help:      "The duration of the last completion in seconds.",
	})

	w.resultMetric = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: w.c.Namespace,
		Name:      "last_completion_successful",
		Help:      "The result of the last completion.",
	})

	w.successTimeMetric = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: w.c.Namespace,
		Name:      "last_success_timestamp_seconds",
		Help:      "The timestamp of the last successful completion.",
	})

	w.completionTimeMetric = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: w.c.Namespace,
		Name:      "last_completion_timestamp_seconds",
		Help:      "The timestamp of the last completion, successful or not.",
	})

	w.pusher = w.pusher.
		Collector(w.durationMetric).
		Collector(w.resultMetric).
		Collector(w.completionTimeMetric)
}

func (w *Wrapper) Run(command []string) (time.Duration, error) {
	start := time.Now()
	execErr := w.exec(command)
	duration := time.Since(start)

	w.completionTimeMetric.SetToCurrentTime()
	w.durationMetric.Set(duration.Seconds())

	if execErr != nil {
		w.resultMetric.Set(0)
	} else {
		w.resultMetric.Set(1)
		w.pusher.Collector(w.successTimeMetric)
		w.successTimeMetric.SetToCurrentTime()
	}

	if !w.pushMetrics {
		return duration, execErr
	}

	if err := w.pusher.Push(); err != nil {
		return duration, fmt.Errorf("Could not push to Pushgateway: %v", err)
	}

	return duration, execErr
}

func (w *Wrapper) exec(command []string) error {
	if len(command) == 0 {
		return errors.New("Command must be specified")
	}

	var cmd *exec.Cmd
	ctx, cancel := context.WithTimeout(context.Background(), w.c.timeout)
	defer cancel()

	if len(command) == 1 {
		cmd = exec.CommandContext(ctx, command[0])
	} else {
		cmd = exec.CommandContext(ctx, command[0], command[1:]...)
	}

	cmd.Env = os.Environ()
	cmd.Stderr = w.log.WriterLevel(logrus.ErrorLevel)
	cmd.Stdout = w.log.WriterLevel(logrus.InfoLevel)

	err := cmd.Run()
	if ctx.Err() == context.DeadlineExceeded {
		return ctx.Err()
	}
	if err != nil {
		return fmt.Errorf("Non-zero exit code: %v", err)
	}
	return nil
}
