package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

type command struct {
	version bool
	config  bool
	cmd     []string
}

var (
	cmd = &command{
		cmd: os.Args[1:],
	}
)

func initFlags() {
	flag.BoolVar(&cmd.version, "version", false, "Prints version and exit")
	flag.BoolVar(&cmd.config, "config", false, "Prints configuration end exit")
	flag.Parse()
}

func realMain(cmd *command) int {
	log := logrus.New()
	log.Out = os.Stdout

	switch strings.ToLower(os.Getenv(outputEnv)) {
	case "text":
		log.Formatter = &logrus.TextFormatter{
			DisableColors: true,
		}
	default:
		log.Formatter = &logrus.JSONFormatter{}
	}

	if cmd.version {
		fmt.Println(Version)
		return 0
	}

	c, err := NewConfig()
	if err != nil {
		fmt.Printf("Configuration error: %v\n", err)
		return 1
	}

	if cmd.config {
		fmt.Println(c)
		return 0
	}

	ctxlog := log.WithFields(c.LogFields())
	ctxlog.Infof("Starting %s...", c.Job)
	w := NewWrapper(ctxlog, c)
	d, err := w.Run(cmd.cmd)
	ctxlog = ctxlog.WithField("duration", d.Seconds()*1000)
	if err != nil {
		ctxlog.Errorf("Done with error: %v", err)
		return 1
	}
	ctxlog.Info("Done")

	return 0
}

func main() {
	initFlags()
	os.Exit(realMain(cmd))
}
