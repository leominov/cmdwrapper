package main

import (
	"net/http"
	"os"
	"testing"
)

func TestRealMain(t *testing.T) {
	flushEnv()
	os.Setenv(jobEnv, "job")
	os.Setenv(pushgatewayEnv, "http://127.0.0.1:9091")
	os.Setenv(timeoutEnv, "0.1d")
	os.Setenv(labelsEnv, "env=prod")
	realMain(&command{false, false, []string{"whoami"}})

	os.Setenv(timeoutEnv, "0.1s")
	realMain(&command{true, true, []string{"whoami"}})
	realMain(&command{false, true, []string{"whoami"}})
	realMain(&command{false, false, []string{"whoami"}})

	go func() {
		mux := http.NewServeMux()
		mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(202)
		})
		err := http.ListenAndServe(":9091", mux)
		if err != nil {
			t.Fatal(err)
		}
	}()

	os.Setenv("CMDWRAPPER_OUTPUT", "text")
	os.Setenv("CMDWRAPPER_PG_URL", "")
	realMain(&command{false, false, []string{"whoami"}})
}

func TestRealMain_2(t *testing.T) {
	initFlags()
}
